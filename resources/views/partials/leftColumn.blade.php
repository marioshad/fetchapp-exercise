
<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  d-flex  align-items-center">
            <a class="navbar-brand" href="{{route('dashboard')}}">
                Fetch App<br> Exercise
            </a>
            <div class=" ml-auto ">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{route('dashboard')}}" class="nav-link">
                            <i class="ni ni-ungroup text-orange"></i>
                            <span class="sidenav-normal"> Dashboard </span>
                        </a>
                    </li>

                    @if(auth()->user()->can('stores-menu-view'))
                        <li class="nav-item">
                            <a class="nav-link" href="#navbar-stores" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-users">
                                <i class="fas fa-boxes text-indigo"></i>
                                <span class="nav-link-text">{{trans('stores.stores_title')}}</span>
                            </a>
                            <div class="collapse" id="navbar-stores">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="{{route('stores.index')}}" class="nav-link">
                                            <span class="sidenav-mini-icon"> S </span>
                                            <span class="sidenav-normal"> {{trans('stores.stores_title')}} </span>
                                        </a>
                                    </li>
                                    @if(auth()->user()->can('stores-create'))
                                        <li class="nav-item">
                                            <a href="{{route('stores.create')}}" class="nav-link">
                                                <span class="sidenav-mini-icon"> CS </span>
                                                <span class="sidenav-normal"> {{trans('stores.create_new')}} </span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif

                    @if(auth()->user()->can('products-index'))
                        <li class="nav-item">
                            <a class="nav-link" href="#navbar-products" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-users">
                                <i class="fas fa-boxes text-indigo"></i>
                                <span class="nav-link-text">{{trans('products.products_title')}}</span>
                            </a>
                            <div class="collapse" id="navbar-products">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="{{route('products.index')}}" class="nav-link">
                                            <span class="sidenav-mini-icon"> P </span>
                                            <span class="sidenav-normal"> {{trans('products.products_title')}} </span>
                                        </a>
                                    </li>
                                    @if(auth()->user()->can('products-create'))
                                        <li class="nav-item">
                                            <a href="{{route('products.create')}}" class="nav-link">
                                                <span class="sidenav-mini-icon"> CP </span>
                                                <span class="sidenav-normal"> {{trans('products.create_new')}} </span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif

                    @if(auth()->user()->can('categories-index'))
                        <li class="nav-item">
                            <a class="nav-link" href="#navbar-product-categories" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-users">
                                <i class="fas fa-network-wired text-indigo"></i>
                                <span class="nav-link-text">{{trans('product-categories.product_categories_title')}}</span>
                            </a>
                            <div class="collapse" id="navbar-product-categories">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="{{route('categories.index')}}" class="nav-link">
                                            <span class="sidenav-mini-icon"> U </span>
                                            <span class="sidenav-normal"> {{trans('product-categories.product_categories_title')}} </span>
                                        </a>
                                    </li>
                                    @if(auth()->user()->can('categories-create'))
                                        <li class="nav-item">
                                            <a href="{{route('categories.create')}}" class="nav-link">
                                                <span class="sidenav-mini-icon"> CU </span>
                                                <span class="sidenav-normal"> {{trans('product-categories.create_new')}} </span>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</nav>
