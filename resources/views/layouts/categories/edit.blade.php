@extends('layouts.app')

@section('header')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">{{trans('product-categories.create_new')}}</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                @can('dashboard_view')
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                                @endcan
                                @can('categories-index')
                                    <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">{{trans('product-categories.product_categories_title')}}</a></li>
                                @endcan
                                @can('categories-edit')
                                    <li class="breadcrumb-item active" aria-current="page">{{trans('product-categories.save')}}</li>
                                @endcan
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        @can('categories-index')
                            <a href="{{ route('categories.index') }}" class="btn btn-sm btn-neutral">Back</a>
                        @endcan
                            @can('categories-edit')
                            <button type="submit" id="form-submit" form="category_edit_form" class="btn btn-sm btn-neutral">{{ trans('product-categories.save_btn') }}</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-frame">
                <div class="card-body">

                    <form id="category_edit_form" action="{{route('categories.update',['category' => $data->id])}}" method="put">

                        @csrf

                        {{-- Text Input --}}
                        <div class="form-group col-12 col-md-6">

                            {{-- Input Label --}}
                            <label id="category_name_label" class="form-control-label" for="category_name">Category Name</label>

                            {{-- Input --}}
                            <input type="text" class="form-control" name="category_name" id="category_name" placeholder="e.g. Shoes" aria-describedby="category_name_help" value="{{$data->name}}">

                            {{-- Help Message --}}
                            <small id="category_name_help" class="form-text text-muted">Please add category name</small>

                            {{-- Validation Failed --}}
                            <div id="invalid_category_name" class="invalid-feedback"></div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')

    <script src="{{asset('/assets/vendor/select2/dist/js/select2.min.js')}}"></script>

    <script>

        /** Form variable */
        let form = $("#category_edit_form");

        /** Handle form submit on click */
        $('#form-submit').on('click', function (e){

            //Prevent form submission
            e.preventDefault();

            console.log('asdfasdfasdfasdfasdf')

            //Post form with AJAX
            postForm();
        });


        function postForm(){

            $.ajax({
                url: '{{route('categories.update',['category' => $data->id])}}',
                type:'patch',
                data:form.serialize(),
                beforeSend:function (){
                    $('.invalid-feedback').html('');
                },
                success:function(response){

                    if (response.errors){

                        /** Loop errors */
                        $.each(response.errors, function (errorIndex, error){

                            /** Add invalid class to input element */
                            $('#'+errorIndex).addClass('invalid');

                            /** Loop error messages */
                            $.each(error, function (messageIndex, error){
                                $('#invalid_'+errorIndex).addClass('show').append('<span class="error-message">'+error+'</span>')
                            });

                        })
                    } else if (response.url){
                        window.location=response.url;
                    }
                },
            });

        }

    </script>

@endsection
