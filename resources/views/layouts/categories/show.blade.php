@extends('layouts.app')

@section('header')

    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">{{trans('product-categories.product_category_title')}}</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                @can('dashboard_view')
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                                @endcan
                                @can('categories-index')
                                    <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">{{trans('product-categories.product_categories_title')}}</a></li>
                                @endcan
                                @can('categories-create')
                                    <li class="breadcrumb-item active" aria-current="page">{{trans('product-categories.view')}}</li>
                                @endcan
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        @can('categories-index')
                            <a href="{{ route('categories.index') }}" class="btn btn-sm btn-neutral">Back</a>
                        @endcan
                        @can('categories-create')
                                <a href="{{ route('categories.edit',['category' => $data->id]) }}" class="btn btn-sm btn-neutral">Edit</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')

    <div class="card mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="form-control-label">Category Name: </label>
                        <span class="d-block">{!! $data->name !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
