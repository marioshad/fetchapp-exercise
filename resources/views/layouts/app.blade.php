<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="{{config('app.name')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('meta')
        <link rel="icon" href="" type="image/png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
        <link rel="stylesheet" href="{{asset('assets/vendor/nucleo/css/nucleo.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/theme/app.css')}}" type="text/css">
        @yield('header_css')
        @yield('header_scripts')
        @stack('extra-styles')

    </head>
    <body class="g-sidenav-show g-sidenav-pinned">

    @include('partials.leftColumn')
    <!-- Main content -->
    <div class="main-content" id="panel">
        @include('partials.topMenu')
        @yield('header')
        <div class="container-fluid pt-4">
            @yield('content')
        </div>
    </div>
        <!-- Core -->
        <script src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('assets/vendor/js-cookie/js.cookie.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
        <!-- Argon JS -->
        <script src="{{asset('js/app.js')}}"></script>
        @yield('footer_scripts')
        @stack('extra-scripts')

    </body>
</html>
