@extends('layouts.app')

@section('header')

    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">{{trans('stores.stores_title')}}</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                @can('dashboard_view')
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fas fa-home"></i></a></li>
                                @endcan
                                @can('stores-index')
                                    <li class="breadcrumb-item"><a href="{{ route('stores.index') }}">{{trans('stores.stores_title')}}</a></li>
                                @endcan
                                @can('stores-create')
                                    <li class="breadcrumb-item active" aria-current="page">{{trans('stores.view')}}</li>
                                @endcan
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        @can('stores-index')
                            <a href="{{ route('stores.index') }}" class="btn btn-sm btn-neutral">Back</a>
                        @endcan
                        @can('stores-create')
                                <a href="{{ route('stores.edit',['store' => $data->id]) }}" class="btn btn-sm btn-neutral">Edit</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')

    <div class="card mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    @if(isset($data->name))
                        <div class="form-group">
                            <label class="form-control-label">Product Name: </label>
                            <span class="d-block">{!! $data->name !!}</span>
                        </div>
                    @endif
                    @if(isset($data->price))
                        <div class="form-group">
                            <label class="form-control-label">Product Price: </label>
                            <span class="d-block">&euro;{!! number_format($data->price, 2) !!}</span>
                        </div>
                    @endif
                    @if(isset($data->category))
                        <div class="form-group">
                            <label class="form-control-label">Category Name: </label>
                            <span class="d-block">{!! $data->category->name !!}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
