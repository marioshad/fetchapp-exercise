@extends('layouts.app')

@push('extra-styles')

    <link href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">

@endpush

@section('header')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">{{trans('stores.stores_title')}}</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                @can('dashboard_view')
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                                @endcan
                                @can('stores-index')
                                    <li class="breadcrumb-item"><a href="{{ route('stores.index') }}">{{trans('stores.stores_title')}}</a></li>
                                @endcan
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        @can('stores-index')
                            <a href="{{ route('stores.index') }}" class="btn btn-sm btn-neutral">Back</a>
                        @endcan
                        @can('stores-create')
                            <a href="{{route('stores.create')}}" class="btn btn-sm btn-neutral">{{ trans('stores.create_btn') }}</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-frame">
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <div>
                                        <table id="table-stores" class="table align-items-center">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th scope="col" class="sort" data-sort="id">ID</th>
                                                    <th scope="col" class="sort" data-sort="name">Name</th>
                                                    <th scope="col" class="sort" data-sort="name">Products</th>
                                                    <th scope="col"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="list">

                                            @foreach($data->stores as $store)

                                                <tr>
                                                    <th scope="row">
                                                        @if(isset($store->id)){{$store->id}}@endif
                                                    </th>

                                                    <td class="name">
                                                        @if(isset($store->name)){{$store->name}}@endif
                                                    </td>

                                                    <td class="name">
                                                        @if(isset($store->products)){{$store->products->count()}}@endif
                                                    </td>

                                                    <td class="text-right">
                                                        <div class="dropdown">
                                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fas fa-ellipsis-v"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                                <a class="dropdown-item" href="{{ route('stores.show',['store' => $store->id]) }}">View</a>
                                                                <a class="dropdown-item" href="{{ route('stores.edit',['store' => $store->id]) }}">Edit</a>
                                                            </div>
                                                        </div>
                                                    </td>

                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('extra-scripts')

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready( function () {
            $('#table-stores').DataTable();
        } );
    </script>

@endpush
