@extends('layouts.app')

@section('header')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">{{trans('stores.create_new')}}</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                @can('dashboard_view')
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                                @endcan
                                @can('stores-index')
                                    <li class="breadcrumb-item"><a href="{{ route('stores.index') }}">{{trans('stores.stores_title')}}</a></li>
                                @endcan
                                @can('stores-edit')
                                    <li class="breadcrumb-item active" aria-current="page">{{trans('stores.save')}}</li>
                                @endcan
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        @can('stores-index')
                            <a href="{{ route('stores.index') }}" class="btn btn-sm btn-neutral">Back</a>
                        @endcan
                            @can('stores-edit')
                            <button type="submit" id="form-submit" form="product_edit_form" class="btn btn-sm btn-neutral">{{ trans('stores.save_btn') }}</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-frame">
                <div class="card-body">

                    <form id="store_edit_form" action="{{route('stores.update',['store' => $store->id])}}" method="put">

                        @csrf

                        {{-- Text Input --}}
                        <div class="form-group col-12 col-md-4">

                            {{-- Input Label --}}
                            <label id="store_name_label" class="form-control-label" for="store_name">Store Name</label>

                            {{-- Input --}}
                            <input type="text" class="form-control" name="store_name" id="store_name" placeholder="e.g. ShoeBox" aria-describedby="store_name_help" value="{{$store->name}}">

                            {{-- Help Message --}}
                            <small id="store_name_help" class="form-text text-muted">Please add store name</small>

                            {{-- Validation Failed --}}
                            <div id="invalid_store_name" class="invalid-feedback"></div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')

    <script src="{{asset('/assets/vendor/select2/dist/js/select2.min.js')}}"></script>

    <script>

        /** Form variable */
        let form = $("#store_edit_form");

        /** Handle form submit on click */
        $('#form-submit').on('click', function (e){

            //Prevent form submission
            e.preventDefault();

            //Post form with AJAX
            postForm();
        });


        function postForm(){

            $.ajax({
                url: '{{route('stores.update',['store' => $store->id])}}',
                type:'patch',
                data:form.serialize(),
                beforeSend:function (){
                    $('.invalid-feedback').html('');
                },
                success:function(response){

                    if (response.errors){

                        /** Loop errors */
                        $.each(response.errors, function (errorIndex, error){

                            /** Add invalid class to input element */
                            $('#'+errorIndex).addClass('invalid');

                            /** Loop error messages */
                            $.each(error, function (messageIndex, error){
                                $('#invalid_'+errorIndex).addClass('show').append('<span class="error-message">'+error+'</span>')
                            });

                        })
                    } else if (response.url){
                        window.location=response.url;
                    }
                },
            });

        }

    </script>

@endsection
