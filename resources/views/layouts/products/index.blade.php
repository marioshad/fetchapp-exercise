@extends('layouts.app')

@push('extra-styles')

    <link href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">

@endpush

@section('header')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">{{trans('products.products_title')}}</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                @can('dashboard_view')
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                                @endcan
                                @can('products-index')
                                    <li class="breadcrumb-item"><a href="{{ route('products.index') }}">{{trans('products.products_title')}}</a></li>
                                @endcan
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        @can('products-index')
                            <a href="{{ route('products.index') }}" class="btn btn-sm btn-neutral">Back</a>
                        @endcan
                        @can('products-create')
                            <a href="{{route('products.create')}}" class="btn btn-sm btn-neutral">{{ trans('products.create_btn') }}</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-frame">
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <div>
                                        <table id="table-products" class="table align-items-center">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th scope="col" class="sort" data-sort="id">ID</th>
                                                    <th scope="col" class="sort" data-sort="name">Name</th>
                                                    <th scope="col" class="sort" data-sort="name">Price</th>
                                                    <th scope="col" class="sort" data-sort="products">Category</th>
                                                    <th scope="col" class="sort" data-sort="stores">Store</th>
                                                    <th scope="col"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="list">


                                            @if(Auth::user()->roles->first()->name == 'super-admin')

                                                @foreach($data->products as $product)

                                                    <tr>
                                                        <th scope="row">
                                                            @if(isset($product->id)){{$product->id}}@endif
                                                        </th>

                                                        <td class="name">
                                                            @if(isset($product->name)){{$product->name}}@endif
                                                        </td>

                                                        <td class="products">
                                                            @if(isset($product->price))&euro;{{number_format($product->price,2)}}@endif
                                                        </td>

                                                        <td class="products">
                                                            @if(isset($product->category)){{$product->category->name}}@endif
                                                        </td>

                                                        <td class="products">
                                                            @if(isset($product->store)){{$product->store->name}}@endif
                                                        </td>

                                                        <td class="text-right">
                                                            <div class="dropdown">
                                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="fas fa-ellipsis-v"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                                    <a class="dropdown-item" href="{{ route('products.show',['product' => $product->id]) }}">View</a>
                                                                    <a class="dropdown-item" href="{{ route('products.edit',['product' => $product->id]) }}">Edit</a>
                                                                </div>
                                                            </div>
                                                        </td>

                                                    </tr>

                                                @endforeach

                                            @elseif(Auth::user()->roles->first()->name == 'store-manager')

                                                @foreach($data->stores as $store)

                                                    @foreach($store->products as $product)

                                                        <tr>
                                                            <th scope="row">
                                                                @if(isset($product->id)){{$product->id}}@endif
                                                            </th>

                                                            <td class="name">
                                                                @if(isset($product->name)){{$product->name}}@endif
                                                            </td>

                                                            <td class="products">
                                                                @if(isset($product->price))&euro;{{number_format($product->price,2)}}@endif
                                                            </td>

                                                            <td class="products">
                                                                @if(isset($product->category)){{$product->category->name}}@endif
                                                            </td>

                                                            <td class="products">
                                                                @if(isset($store->name)){{$store->name}}@endif
                                                            </td>

                                                            <td class="text-right">
                                                                <div class="dropdown">
                                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        <i class="fas fa-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                                        <a class="dropdown-item" href="{{ route('products.show',['product' => $product->id]) }}">View</a>
                                                                        <a class="dropdown-item" href="{{ route('products.edit',['product' => $product->id]) }}">Edit</a>
                                                                    </div>
                                                                </div>
                                                            </td>

                                                        </tr>

                                                    @endforeach

                                                @endforeach

                                            @endif

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('extra-scripts')

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready( function () {
            $('#table-products').DataTable();
        } );
    </script>

@endpush
