@extends('layouts.app')

@section('header')
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">{{trans('products.create_new')}}</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                @can('dashboard_view')
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                                @endcan
                                @can('products-index')
                                    <li class="breadcrumb-item"><a href="{{ route('products.index') }}">{{trans('products.products_title')}}</a></li>
                                @endcan
                                @can('products-create')
                                    <li class="breadcrumb-item active" aria-current="page">{{trans('products.create_new')}}</li>
                                @endcan
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        @can('products-index')
                            <a href="{{ route('products.index') }}" class="btn btn-sm btn-neutral">Back</a>
                        @endcan
                            @can('products-create')
                            <button type="submit" id="form-submit" form="product_create_form" class="btn btn-sm btn-neutral">{{ trans('products.create_btn') }}</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-frame">
                <div class="card-body">

                    <form id="product_create_form" action="{{ route('products.store') }}" method="POST">

                        @csrf

                        <div class="form-row">

                            {{-- Text Input --}}
                            <div class="form-group col-12 col-md-4">

                                {{-- Input Label --}}
                                <label id="product_name_label" class="form-control-label" for="product_name">Product Name</label>

                                {{-- Input --}}
                                <input type="text" class="form-control" name="product_name" id="product_name" placeholder="e.g. Shoes" aria-describedby="product_name_help">

                                {{-- Help Message --}}
                                <small id="product_name_help" class="form-text text-muted">Please add product name</small>

                                {{-- Validation Failed --}}
                                <div id="invalid_product_name" class="invalid-feedback"></div>

                            </div>

                            {{-- Number Input --}}
                            <div class="form-group col-12 col-md-4">

                                {{-- Input Label --}}
                                <label id="product_price_label" class="form-control-label" for="product_price">Product Price</label>

                                {{-- Input --}}
                                <input type="number" min="0" class="form-control" name="product_price" id="product_price" aria-describedby="product_price_help">

                                {{-- Help Message --}}
                                <small id="product_price_help" class="form-text text-muted">Please add product price</small>

                                {{-- Validation Failed --}}
                                <div id="invalid_product_price" class="invalid-feedback"></div>

                            </div>


                            {{-- Select Input --}}
                            <div class="form-group col-12 col-md-4">

                                {{-- Input Label --}}
                                <label for="product_category_id" class="form-control-label">Product Category</label>

                                {{-- Input --}}
                                <select
                                        class="form-control select2"
                                        aria-describedby="product_category_help"
                                        name="product_category_id"
                                        id="product_category_id"
                                        data-toggle="select"
                                >
                                    <option value="-1">Select</option>
                                    @if(isset($data))
                                        @foreach($data as $key => $productCategory)
                                            <option value="{{$productCategory->id}}">{{$productCategory->name}}</option>
                                        @endforeach
                                    @endif

                                </select>

                                {{-- Help Message --}}
                                <small id="product_category_id_help" class="form-text text-muted">Select product category</small>

                                {{-- Validation Failed --}}
                                <div id="invalid-product_category_id" class="invalid-feedback"></div>

                            </div>

                            {{-- Select Input --}}
                            <div class="form-group col-12 col-md-4">

                                {{-- Input Label --}}
                                <label for="product_store_id" class="form-control-label">Store</label>

                                {{-- Input --}}
                                <select
                                        class="form-control select2"
                                        aria-describedby="store_help"
                                        name="store_id"
                                        id="store_id"
                                        data-toggle="select"
                                >
                                    <option value="-1">Select</option>
                                    @if(isset($stores))
                                        @foreach($stores as $key => $store)
                                            <option value="{{$store->id}}">{{$store->name}}</option>
                                        @endforeach
                                    @endif

                                </select>

                                {{-- Help Message --}}
                                <small id="store_id_help" class="form-text text-muted">Select Store</small>

                                {{-- Validation Failed --}}
                                <div id="invalid-store_id" class="invalid-feedback"></div>

                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')

    <script>

        /** Form variable */
        let form = $("#product_create_form");

        /** Handle form submit on click */
        $('#form-submit').on('click', function (e){

            //Prevent form submission
            e.preventDefault();

            //Post form with AJAX
            postForm();

        });


        function postForm(){

            $.ajax({
                url: '{{route('products.store')}}',
                type:'POST',
                data:form.serialize(),
                beforeSend:function (){
                    $('.invalid-feedback').html('');
                },
                success:function(response){

                    if (response.errors){

                        /** Loop errors */
                        $.each(response.errors, function (errorIndex, error){

                            /** Add invalid class to input element */
                            $('#'+errorIndex).addClass('invalid');

                            /** Loop error messages */
                            $.each(error, function (messageIndex, error){
                                $('#invalid_'+errorIndex).addClass('show').append('<span class="error-message">'+error+'</span>')
                            });

                        })
                    } else if (response.url){
                        window.location=response.url;
                    }
                },
            });

        }

    </script>

@endsection
