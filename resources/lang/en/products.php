<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Products Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'create_new' => 'Create New Product',
    'create_btn'    => 'Create',
    'save_btn'    => 'Save',
    'products_title' => 'Products',
    'password' => 'Password',
    'created_at' => 'Created Date',
    'view' => 'View',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'previous' => 'Previous',
    'next' => 'Next',
    'search_products' => 'Search Products',

];
