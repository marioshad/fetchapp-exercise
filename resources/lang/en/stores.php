<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Stores Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'create_new' => 'Create New Store',
    'create_btn'    => 'Create',
    'save_btn'    => 'Save',
    'stores_title' => 'Stores',
    'created_at' => 'Created Date',
    'view' => 'View',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'previous' => 'Previous',
    'next' => 'Next',
    'search_stores' => 'Search Stores',

];
