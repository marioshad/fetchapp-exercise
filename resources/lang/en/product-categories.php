<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product Categories Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'create_new' => 'Create New Category',
    'create_btn'    => 'Create',
    'save_btn'    => 'Save',
    'product_categories_title' => 'Product Categories',
    'product_category_title' => 'Product Category',
    'password' => 'Password',
    'created_at' => 'Created Date',
    'view' => 'View',
    'edit' => 'Edit',
    'save' => 'Save',
    'delete' => 'Delete',
    'previous' => 'Previous',
    'next' => 'Next',
    'search_product_categories' => 'Search Product Categories',

];
