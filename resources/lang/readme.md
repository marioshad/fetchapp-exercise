# Localization

> Please seed full documentation below
> 
> https://laravel.com/docs/8.x/localization

##  Table of Contents

- Users

##Users
Users localization file structure

### Usage

#### Layout
Users page layout language strings:
```sh

    /*
    |--------------------------------------------------------------------------
    | Users Language Lines
    |--------------------------------------------------------------------------
    */

    'create_new' => 'Create New User',
    'create_btn'    => 'Create',
    'users_title' => 'Users',
    'password' => 'Password',
    'created_at' => 'Created Date',
    'view' => 'View',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'previous' => 'Previous',
    'next' => 'Next',
    'search_users' => 'Search Users',
```

#### Tabs 
```sh
    /*
    |--------------------------------------------------------------------------
    | Tabs
    |--------------------------------------------------------------------------
    */

    //  Account
    'tab_details_title'     =>  'Details',
    'tab_location_title'    =>  'Location',
```

#### Sections
```sh
    /*
    |--------------------------------------------------------------------------
    | Section
    |--------------------------------------------------------------------------
    */

    //  Section 1
    'section_title'   =>  'Section 1',
    'section_desc'    =>  'Section 1 short description',
    
    //  Section 2
    'section_title'   =>  'Section 2',
    ...
```

#### Fields
```sh
    /*
    |--------------------------------------------------------------------------
    | Fields
    |--------------------------------------------------------------------------
    */
```
