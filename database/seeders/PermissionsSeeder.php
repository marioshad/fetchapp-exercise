<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = array(
            'admin-dashboard-view',

            'stores-menu-view',
            'stores-index',
            'stores-view',
            'stores-edit',
            'stores-create',
            'stores-delete',

            'products-menu-view',
            'products-view',
            'products-index',
            'products-edit',
            'products-create',
            'products-delete',

            'categories-menu-view',
            'categories-view',
            'categories-index',
            'categories-edit',
            'categories-create',
            'categories-delete',

            'roles-view',
            'roles-index',
            'roles-edit',
            'roles-create',
            'roles-delete',
        );

        foreach ($permissions as $permission) {

            Permission::create(['name' => $permission]);

        }
    }
}
