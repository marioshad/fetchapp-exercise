<?php

namespace Database\Seeders;

use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productCategories = [
            ['name' => 'Shows'],
            ['name' => 'Kitchen'],
            ['name' => 'Convenience Store'],
            ['name' => 'Organic Food'],
            ['name' => 'Sports'],
        ];

        foreach ($productCategories as $productCategory) {
            ProductCategory::create($productCategory);
        }
    }
}
