<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_id = DB::table('users')->insertGetId([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('secret')
        ]);

        $user = User::find($user_id);
        $user->assignRole('super-admin');

        $user_id = DB::table('users')->insertGetId([
            'name' => 'Store Manager 1',
            'email' => 'storemanager1@store.com',
            'password' => Hash::make('storemanager1')
        ]);

        $user = User::find($user_id);
        $user->assignRole('store-manager');

        $user_id = DB::table('users')->insertGetId([
            'name' => 'Store Manager 2',
            'email' => 'storemanager2@store.com',
            'password' => Hash::make('storemanager2')
        ]);

        $user = User::find($user_id);
        $user->assignRole('store-manager');

    }
}
