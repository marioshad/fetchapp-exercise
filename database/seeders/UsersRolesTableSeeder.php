<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin_permissions = array(
            'admin-dashboard-view',

            'stores-menu-view',
            'stores-index',
            'stores-view',
            'stores-edit',
            'stores-create',
            'stores-delete',

            'products-menu-view',
            'products-view',
            'products-index',
            'products-edit',
            'products-create',
            'products-delete',

            'categories-menu-view',
            'categories-view',
            'categories-index',
            'categories-edit',
            'categories-create',
            'categories-delete',

            'roles-view',
            'roles-index',
            'roles-edit',
            'roles-create',
            'roles-delete',
        );

        $role1 = Role::create(['name' => 'super-admin']);
        foreach ($superadmin_permissions as $permission) {
            $role1->givePermissionTo($permission);
        }

        $admin_permissions = array(
            'admin-dashboard-view',

            'stores-menu-view',
            'stores-index',
            'stores-view',
            'stores-edit',
            'stores-create',
            'stores-delete',

            'products-menu-view',
            'products-view',
            'products-index',
            'products-edit',
            'products-create',
            'products-delete',

            'categories-menu-view',
            'categories-view',
            'categories-index',
            'categories-edit',
            'categories-create',
            'categories-delete',

        );

        $role2 = Role::create(['name' => 'admin']);
        foreach ($admin_permissions as $permission) {
            $role2->givePermissionTo($permission);
        }

        $store_manager_permissions = array(
            'admin-dashboard-view',

            'stores-menu-view',
            'stores-index',
            'stores-view',
            'stores-edit',
            'stores-create',
            'stores-delete',

            'products-menu-view',
            'products-view',
            'products-index',
            'products-edit',
            'products-create',
            'products-delete',

        );

        $role3 = Role::create(['name' => 'store-manager']);
        foreach ($store_manager_permissions as $permission) {
            $role3->givePermissionTo($permission);
        }

    }
}
