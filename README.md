## Download repository and install the composer dependencies

```https://bitbucket.org/marioshad/fetchapp-exercise.git```

```composer install```

## Configure .env file

## Migrate tables
```php artisan migrate```

## Seed tables

Also, I created a database seeder for user roles and categories.

```php artisan db:seed```


## Demo Accounts

The default registration role is store manager, so you can create more stores if you like. Store managers can only create/edit products and admin can create/edit categories.

### Admin
Role: Super Admin

Email: admin@admin.com

Password: secret

### Fetch Shop 1
Role: Store Manager

Email: storemanager1@store.com

Password: storemanager1

### Fetch Shop 2  
Role: Store Manager

Email: storemanager2@ store.com

Password: storemanager2

## API

I created three API routes to retrieve all stores with their products, all products with their relationships and a single product

```/api/v1/stores```

```/api/v1/products```

```/api/v1/products/9```