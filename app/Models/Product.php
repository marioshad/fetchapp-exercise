<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'category_id'
    ];

    /**
     * Get the category associated with the product.
     *
     */
    public function category()
    {
        return $this->hasOneThrough(
            ProductCategory::class,
            MapProductCategory::class,
            'product_id', // Foreign key on the cars table...
            'id', // Foreign key on the owners table...
            'id', // Local key on the mechanics table...
            'category_id' // Local key on the cars table...
            );
    }

    /**
     * Get the category associated with the product.
     *
     */
    public function store()
    {
        return $this->hasOneThrough(
            Store::class,
            MapStoreProducts::class,
            'product_id',
            'id',
            'id',
            'store_id'
        );
    }

}
