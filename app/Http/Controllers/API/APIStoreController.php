<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class APIStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {

        $stores = Store::with('products', 'products.category')->get();

        return $stores;
    }

}
