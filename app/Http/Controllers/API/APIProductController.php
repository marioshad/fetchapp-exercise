<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Http\Request;

class APIProductController extends Controller
{
    /**
     * Display a listing of the products.
     *
     */
    public function index()
    {

        $products = Product::with('store', 'category')->get();

        return $products;
    }

    /**
     * Display the specified product.
     *
     * @param int $id
     */
    public function show(int $id)
    {

        $product = Product::with('store','category',)->find($id);

        return $product;
    }

}
