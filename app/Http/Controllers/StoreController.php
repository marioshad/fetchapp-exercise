<?php

namespace App\Http\Controllers;

use App\Models\MapProductCategory;
use App\Models\MapStoreProducts;
use App\Models\MapUserStore;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Store;
use App\Models\StoreProducts;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        if ( Auth::user()->hasRole('super-admin')) {

            $stores = Store::with('products')->get();

            $data = (object) [
                'stores' => $stores
            ];

            return view('layouts.stores.index')->with([
                'data' => $data
            ]);
        }

        $data = User::where('id','=', Auth::id())->with('stores', 'stores.products')->first();

        return view('layouts.stores.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('layouts.stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $newStore = null;

        $messages = [
            'store_name.required' => "Store name is required",
        ];

        $rules = [
            'store_name' => 'required|max:255|string',
        ];

        $validator = Validator::make($request->all(),$rules,$messages);


        if ($validator->fails()){

            return response()->json([
                'errors'    =>  $validator->getMessageBag()
            ]);

        } else {

            DB::beginTransaction();

            try {

                $newStore = Store::create([
                    'name'                  => $validator->valid()['store_name'],
                ]);

                MapUserStore::create([
                    'user_id'      =>   Auth::id(),
                    'store_id'    =>   $newStore->id,
                ]);

            } catch (\Exception $e){

                Log::error($e->getMessage());

                DB::rollBack();

            }

            DB::commit();

            return response()->json(['url' => route('stores.show',['store' => $newStore->id])]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        $data = Store::find($id);

        return view('layouts.stores.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $store = Store::find($id);


        return view('layouts.stores.edit')->with([
            'store' => $store,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'store_name.required' => "Store name is required",
        ];

        $rules = [
            'store_name' => 'required|max:255|string',
        ];

        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails()){

            return response()->json([
                'errors'    =>  $validator->getMessageBag()
            ]);

        } else {

            DB::beginTransaction();

            try {

                Store::where('id', $id)->update([
                    'name'                  => $validator->valid()['store_name'],
                ]);

            } catch (\Exception $e){

                Log::error($e->getMessage());

                DB::rollBack();

            }

            DB::commit();

            $store = Store::find($id);

        }

        return response()->json(['url' => route('stores.show',['store' => $store->id])]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
