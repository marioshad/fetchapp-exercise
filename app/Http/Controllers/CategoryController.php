<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {

        $productCategories = ProductCategory::all();

        return view('layouts.categories.index')->with([
            'data' => $productCategories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|View
     */
    public function create()
    {
        return view('layouts.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $newCategory = null;

        $messages = [
            'category_name.required' => "Category name is required",
            'category_name.regex' => "Category name is not valid",
            'category_name.unique' => "Category name is exists",
        ];

        $rules = [
            'category_name'      =>  'required|max:255|string|regex:/^[\pL\s\-]+$/u',
        ];

        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails()){

            return response()->json([
                'errors'    =>  $validator->getMessageBag()
            ]);

        } else {

            DB::beginTransaction();

            try {

                $newCategory = ProductCategory::create([
                    'name'                  => $validator->valid()['category_name'],
                ]);

            } catch (\Exception $e){

                Log::error($e->getMessage());

                DB::rollBack();

            }

            DB::commit();

        }

        return response()->json(['url' => route('categories.show',['category' => $newCategory->id])]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {

        $data = ProductCategory::find($id);

        return view('layouts.categories.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ProductCategory::find($id);

        return view('layouts.categories.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     */
    public function update(Request $request, int $id)
    {

        $messages = [
            'category_name.required' => "Category name is required",
            'category_name.regex' => "Category name is not valid",
            'category_name.unique' => "Category name is exists",
        ];

        $rules = [
            'category_name'      =>  'required|max:255|string|regex:/^[\pL\s\-]+$/u',
        ];

        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails()){

            return response()->json([
                'errors'    =>  $validator->getMessageBag()
            ]);

        } else {

            DB::beginTransaction();

            try {

                ProductCategory::where('id', $id)->update([
                    'name'                  => $validator->valid()['category_name'],
                ]);

            } catch (\Exception $e){

                Log::error($e->getMessage());

                DB::rollBack();

            }

            DB::commit();

            $productCategory = ProductCategory::find($id);

        }

        return response()->json(['url' => route('categories.show',['category' => $productCategory->id])]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
