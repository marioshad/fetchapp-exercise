<?php

namespace App\Http\Controllers;

use App\Models\MapProductCategory;
use App\Models\MapStoreProducts;
use App\Models\MapUserStore;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Store;
use App\Models\StoreProducts;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {

        if ( Auth::user()->hasRole('super-admin')) {

            $products = Product::with('category', 'store')->get();

            $data = (object) [
                'products' => $products
            ];

            return view('layouts.products.index')->with([
                'data' => $data
            ]);
        }

        $data = User::where('id','=', Auth::id())->with('stores', 'stores.products', 'stores.products.category')->first();

        return view('layouts.products.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {

        $productCategories = ProductCategory::all();

        $user = User::with('stores')->find(Auth::id());

        return view('layouts.products.create')->with([
            'data' => $productCategories,
            'stores' => $user->stores,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $newProduct = null;

        $messages = [
            'product_name.required' => "Product name is required",
            'product_price.required' => "Product price is required",
        ];

        $rules = [
            'product_name' => 'required|max:255|string',
            'product_price' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules,$messages);


        if ($validator->fails()){

            return response()->json([
                'errors'    =>  $validator->getMessageBag()
            ]);

        } else {

            DB::beginTransaction();

            try {

                $newProduct = Product::create([
                    'name'                  => $validator->valid()['product_name'],
                    'price'                 => $validator->valid()['product_price'],
                ]);

                MapStoreProducts::create([
                    'product_id'      =>   $newProduct->id,
                    'store_id'    =>   $validator->valid()['store_id'],
                ]);

                MapProductCategory::create([
                    'product_id'     =>   $newProduct->id,
                    'category_id'    =>   $validator->valid()['product_category_id']
                ]);

            } catch (\Exception $e){

                Log::error($e->getMessage());

                DB::rollBack();

            }

            DB::commit();


            return response()->json(['url' => route('products.show',['product' => $newProduct->id])]);

        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Product::with('category','store')->find($id);

        return view('layouts.products.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::with('category', 'store')->find($id);

        $productCategories = ProductCategory::all();

        $user = User::with('stores')->find(Auth::id());

        return view('layouts.products.edit')->with([
            'product' => $product,
            'data' => $productCategories,
            'stores' => $user->stores,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {

        $messages = [
            'product_name.required' => "Product name is required",
            'product_price.required' => "Product price is required",
        ];

        $rules = [
            'product_name' => 'required|max:255|string',
            'product_price' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails()){

            return response()->json([
                'errors'    =>  $validator->getMessageBag()
            ]);

        } else {

            DB::beginTransaction();

            try {

                Product::where('id', $id)->update([
                    'name'                  => $validator->valid()['product_name'],
                    'price'                 => $validator->valid()['product_price'],
                ]);

                MapStoreProducts::where('product_id','=', $id)->update([
                    'store_id'    =>   $validator->valid()['store_id'],
                ]);

                MapProductCategory::where('product_id','=', $id)->update([
                    'category_id'    =>   $validator->valid()['product_category_id']
                ]);

            } catch (\Exception $e){

                Log::error($e->getMessage());

                DB::rollBack();

            }

            DB::commit();

            $product = Product::find($id);

        }

        return response()->json(['url' => route('products.show',['product' => $product->id])]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
