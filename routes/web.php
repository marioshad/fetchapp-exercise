<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');


Route::group(['middleware' => ['auth']], function () {

    /** Store Routes */
    Route::resource('stores', 'App\Http\Controllers\StoreController');

    /** Product Routes */
    Route::resource('stores.products', 'App\Http\Controllers\ProductController');

    /** Product Routes */
    Route::resource('products', 'App\Http\Controllers\ProductController');

    /** Category Routes */
    Route::resource('categories', 'App\Http\Controllers\CategoryController');

});

